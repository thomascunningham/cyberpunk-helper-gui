namespace Cyberpunk_Helper_GUI
{
    using System.Collections.Generic;

    public class ViewAllCharactersModel
    {
        public List<string> Characters { get; set; }
    }
}