namespace Cyberpunk_Helper_GUI.Pages.ViewCharacter
{
    using CyberpunkRed.Io;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.Extensions.Logging;
    using System.Linq;

    public class ViewAllCharacters : PageModel
    {
        public string Handle { get; set; }

        private readonly ILogger<IndexModel> _logger;
        private readonly SaveFile _saveFile;

        public ViewAllCharacters(ILogger<IndexModel> logger, SaveFile saveFile)
        {
            _logger = logger;
            _saveFile = saveFile;
        }

        [BindProperty]
        public ViewAllCharactersModel AllCharacters { get; set; }

        public IActionResult OnGet()
        {
            AllCharacters = new ViewAllCharactersModel();
            AllCharacters.Characters = _saveFile.GetAllSavedCharacters().ToList();

            return Page();
        }
    }
}
