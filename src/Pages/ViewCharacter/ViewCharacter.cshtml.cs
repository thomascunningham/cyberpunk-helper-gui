namespace Cyberpunk_Helper_GUI.Pages.ViewCharacter
{
    using System.Threading.Tasks;
    using CyberpunkRed.Io;
    using CyberpunkRed.Io.Dto;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.Extensions.Logging;

    public class ViewCharacter : PageModel
    {
        public string Handle { get; set; }

        private readonly ILogger<IndexModel> _logger;
        private readonly SaveFile _saveFile;

        public ViewCharacter(ILogger<IndexModel> logger, SaveFile saveFile)
        {
            _logger = logger;
            _saveFile = saveFile;
        }

        [BindProperty]
        public CharacterDto Character { get; set; }

        public async Task<IActionResult> OnGet(string name)
        {
            Character = await _saveFile.GetCharacterDtoAsync(name);

            return Page();
        }
    }
}