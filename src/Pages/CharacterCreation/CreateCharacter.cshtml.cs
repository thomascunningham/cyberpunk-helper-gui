namespace Cyberpunk_Helper_GUI.Pages.CharacterCreation
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.Extensions.Logging;
    using CyberpunkRed.Io;
    using System.Threading.Tasks;

    public class CreateCharacter : PageModel
    {
        public string Handle { get; set; }

        private readonly ILogger<IndexModel> _logger;
        private readonly SaveFile _saveFile;

        public CreateCharacter(ILogger<IndexModel> logger, SaveFile saveFile)
        {
            _logger = logger;
            _saveFile = saveFile;
        }

        [BindProperty]
        public BasicCharacterCreationModel Character { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var character = CyberpunkRed.Character.CreateBasicCharacterSheet(Character.Handle);
            var dto = character.GetDto();

            await _saveFile.SaveCharacterDtoAsync(dto);

            return RedirectToPage("./ChooseSkills", new { name = Character.Handle });
        }
    }
}
