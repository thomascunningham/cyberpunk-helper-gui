namespace Cyberpunk_Helper_GUI.Pages.CharacterCreation
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.Extensions.Logging;
    using CyberpunkRed.Io;
    using System.Threading.Tasks;

    public class ChooseSkills : PageModel
    {

        public string Handle { get; set; }

        private readonly ILogger<IndexModel> _logger;
        private readonly SaveFile _saveFile;

        public ChooseSkills(ILogger<IndexModel> logger, SaveFile saveFile)
        {
            _logger = logger;
            _saveFile = saveFile;
        }

        [BindProperty]
        public BasicCharacterCreationModel Character { get; set; }

        public IActionResult OnGet(string name)
        {
            Character = new BasicCharacterCreationModel
            {
                Handle = name
            };
            
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var createdCharacter = Character.ToBasicCharacter();
            createdCharacter.DeriveHealthAndHumanity();
            var dto = createdCharacter.GetDto();

            await _saveFile.SaveCharacterDtoAsync(dto);

            return RedirectToPage("/Index");
        }
    }
}
