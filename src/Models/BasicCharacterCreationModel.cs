namespace Cyberpunk_Helper_GUI
{
    using System.ComponentModel.DataAnnotations;
    using CyberpunkRed;

    public class BasicCharacterCreationModel
    {
        [Required, StringLength(30, MinimumLength = 1)]
        public string Handle { get; set; }

        [Required]
        public Role Role { get; set; }

        [Required]
        [Range(1, 8)]
        public int Int { get; set; }

        [Required]
        [Range(1, 8)]
        public int Ref { get; set; }

        [Required]
        [Range(1, 8)]
        public int Dex { get; set; }

        [Required]
        [Range(1, 8)]
        public int Tech { get; set; }

        [Required]
        [Range(1, 8)]
        public int Cool { get; set; }

        [Required]
        [Range(1, 8)]
        public int Will { get; set; }

        [Required]
        [Range(1, 8)]
        public int Luck { get; set; }

        [Required]
        [Range(1, 8)]
        public int Move { get; set; }

        [Required]
        [Range(1, 8)]
        public int Body { get; set; }

        [Required]
        [Range(1, 8)]
        public int Emp { get; set; }

        public Character ToBasicCharacter()
        {
            var character = Character.CreateBasicCharacterSheet(Handle);
            character.Stats.SetStatInitialValue("Intelligence", Int);
            character.Stats.SetStatInitialValue("Willpower", Will);
            character.Stats.SetStatInitialValue("Cool", Cool);
            character.Stats.SetStatInitialValue("Empathy", Emp);
            character.Stats.SetStatInitialValue("Technique", Tech);
            character.Stats.SetStatInitialValue("Reflexes", Ref);
            character.Stats.SetStatInitialValue("Luck", Luck);
            character.Stats.SetStatInitialValue("Body", Body);
            character.Stats.SetStatInitialValue("Dexterity", Dex);
            character.Stats.SetStatInitialValue("Movement", Move);

            character.DeriveHealthAndHumanity();

            return character;
        }
    }

    public enum Role
    {
        Rockerboy,
        Solo,
        Netrunner,
        Tech,
        Medtech,
        Media,
        Exec,
        Lawman,
        Fixer,
        Nomad,
    }
}